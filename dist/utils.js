function GetBlockExplorerUrl(s) {
  switch (s) {
  case "eth":
    return "https://etherscan.io/tx/";
  case "etc":
    return "http://gastracker.io/tx/";
  case "zec":
    return "http://zcl.apool.cc/miner/";
  case "sc":
  }
  return "todo";
}

function GetQueryString(name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r !== null) return unescape(r[2]);
  return null;
}

//get query
function GetQueryString(name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r !== null) return unescape(r[2]);
  return null;
}

function Average(arr) {
  if (arr.length === 0) {
    return { avg: 0, now: 0 };
  }
  var total = 0;
  for (var i = 0; i < arr.length; i++) {
    total += arr[i];
  }
  var avg = (total / arr.length).toFixed(2);
  var now = arr[arr.length - 1].toFixed(2);
  return { avg: avg, now: now };
}

Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

function chart(data) {
  var myChart = echarts.init(document.getElementById('minerChart'));
  var history = data.Result.History;
  var option = {
    title: {
      text: '状态统计',
      left: 'center'
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#999'
        }
      }
    },
    legend: {
      data: ['余额', '总算力'],
      bottom: 0
    },
    xAxis: [{
      type: 'category',
      data: history.Time,
      axisPointer: {
        type: 'shadow'
      }
    }],
    yAxis: [{
      type: 'value',
      name: '总算力',
      scale: true,
      axisLabel: {
        formatter: '{value}'
      },
      splitNumber: 8,
      min: function (value) {
        var v = value.min * 0.5;
        if (v < 0) {
          v = 0;
        }
        return v.toFixed(1);
      },
      max: function (value) {
        return (value.max * 1.1).toFixed(1);
      }
    }, {
      show: false,
      type: 'value',
      name: '余额',
      axisLabel: {
        formatter: '{value}'
      },
      splitNumber: 8,
      max: function (value) {
        return value.max * 4;
      }
    }],
    series: [{
      "name": "总算力",
      "type": "line",
      "data": history.Speed,
      "smooth": true,
      "itemStyle": {
        "normal": {
          "lineStyle": {
            "color": "#006633"
          }
        }
      }
    }, {
      "name": "余额",
      "type": "line",
      "itemStyle": {
        "normal": {
          "lineStyle": {
            "color": "#cc6600",
            "width": 1
          },
          "areaStyle": {
            "color": "#cccc66",
            "type": "default"
          }
        }
      },
      "data": history.Balance,
      "yAxisIndex": 1,
      "smooth": true
    }]
  };
  myChart.setOption(option);
}